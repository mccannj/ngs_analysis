#! /usr/bin/env python3

import os, subprocess, shutil

from dna_utils.seqio import PairedFastqReader
# from other_utils.misc_utils import grouper
from other_utils.misc_utils import grouper_witer


def check_qual(fq_obj, minq=10, basep=0.95, ns=0, encoding="Sanger"):

	if encoding == "Sanger":
		rg = (33,74)

	bases = 0
	ncount = 0

	for b,q in zip(fq_obj.seq, fq_obj.qual):

		if b == "N":
			ncount += 1
			if ncount > ns:
				return False

		if ord(q) >= rg[0] + minq:
			bases += 1
	

	return bases/len(fq_obj.seq) >= basep


def bbduk(fqs, outs, fref, stats, proc_op):
	stats = fqs[0].replace('m1.fq', 'bbduk.stats.fq')
	cmd = "bbduk2.sh in=%s in2=%s out=%s out2=%s fref=%s k=15 qin=64 stats=%s -Xmx2g"
	cmd = cmd % (fqs[0], fqs[1], outs[0], outs[1], fref, stats)
	proc_out = open(proc_op, 'wb')
	proc = subprocess.Popen(cmd.split(' '), stdout=proc_out, stderr=proc_out)
	out,err = proc.communicate()
	proc_out.close()
	return 0


def trim_read(fq_obj, end=1, trim=0):
	if end == 1:
		fq_obj.seq = fq_obj.seq[trim:]
                fq_obj.qual = fq_obj.qual[trim:]
	elif end == -1:
		fq_obj.seq = fq_obj.seq[::-1][trim:][::-1]
                fq_obj.qual = fq_obj.qual[::-1][trim:][::-1]
	return fq_obj


# def fq_filt(ret_dict, iter_in, fref, outf_list, trimlen, trimend, ns_allowed):
def fq_filt(ret_dict, iter_in, outf_list, trimlen, trimend, ns_allowed):

	fout1 = open(outf_list[0], "w")
	fout2 = open(outf_list[1], "w")
	id1 = "tmp_seqs/tmp.file.%s.m1.fq" % os.getpid()
	id2 = "tmp_seqs/tmp.file.%s.m2.fq" % os.getpid()

	#fout1 = open("tmp_seqs/tmp.file.%s.m1.fq" % os.getpid())
	#fout2 = open("tmp_seqs/tmp.file.%s.m2.fq" % os.getpid())
		

	paired_fastq_iter = (x for x in iter_in)

	#paired_fastq_iter = iter_in

	seq_count = 0
	bad_count = 0
	good_count = 0
	try:
		for seqf, seqr in paired_fastq_iter:

			seq_count += 2
			checka = check_qual(seqf, ns=ns_allowed)

			if not checka:
				bad_count += 2
				continue
			else:
				checkb = check_qual(seqr, ns=ns_allowed)
				if not checkb:
					bad_count += 2
					continue
			good_count += 2

			if trimlen:
				seqf = trim_read(seqf, end=trimend, trim=trimlen)
				seqr = trim_read(seqr, end=trimend, trim=trimlen)
			#print(seqf.fastq_out(), file=outfiles[0])
			#print(seqr.fastq_out(), file=outfiles[-1])
			print(seqf.fastq_out(), file=fout1)
			print(seqr.fastq_out(), file=fout2)
	
		fout1.close()
		fout2.close()
	except TypeError:
		fout1.close()
		fout2.close()
		#os.remove(outf_list[0])
		#os.remove(outf_list[1])

	# bbduk_outf1 = outf_list[0].replace('m1.fq','bbduk.m1.fq')
	# bbduk_outf2 = outf_list[1].replace('m2.fq','bbduk.m2.fq')
	# stats = outf_list[0].replace('m1.fq','bbduk.stats.txt')
	# prg_op = outf_list[0].replace('m1.fq','bbduk.output.txt')


	# bbduk(outf_list, [bbduk_outf1, bbduk_outf2], fref, stats, prg_op)

	ret_dict[os.getpid()] = (seq_count, good_count, bad_count)
	return (id1, id2)


def main():

	from other_utils.partools import parallel
	import multiprocessing as mp
	import time
	import argparse as ap

	parser = ap.ArgumentParser()
	parser.add_argument('-a', '--fastqa', 
						help="name of fastq file with first mate", required=True, type=str)
	parser.add_argument('-b', '--fastqb',
	 					help="name of fastq file with second mate", required=True, type=str)
	parser.add_argument('-f', '--matef', help="name of output file for first mate",
						required=True, type=str)
	parser.add_argument('-r', '--mater', help="name of output file for second mate",
	 					required=True, type=str)
	parser.add_argument('-i', '--filtnum', help="number of times file has been previously filtered",
	 					required=False, type=str, default="0")
	# parser.add_argument('-i', "--adaptors", help="fasta formatted file with adaptors and informative names",
	# 					required=True, type=str)
	parser.add_argument('-t', '--trim', help="number bases to trim", type=int, default=0, required=False)
	parser.add_argument('-e', '--end', help="end to trim, 1 for left, -1 for right",
						type=int, default=1, required=False)
	parser.add_argument('-q', '--minq', help="min qual thresh for counting nucleotide as good enough",
						type=int, default=10, required=False)
	parser.add_argument('-d', '--basep', help="percent bases per seq required to be above minq thresh",
						type=float, default=0.95, required=False)
	parser.add_argument('-n', '--ns', help="number of N allowed in each seq", type=int, default=0, required=False)
	parser.add_argument('-c', '--cpus', help="number of cpus to use", required=False, type=int, default=mp.cpu_count())

	print(" ")
	start_time = " >> TIME START: %s" % time.strftime("%H:%M:%S %Z")

	args = parser.parse_args()

	manager = mp.Manager()
	ret_dict = manager.dict()

	print(" >> Number of CPUs available: %s" % args.cpus)
	print(" >> Number of CPUs to be used: %s\n" % args.cpus)

	print(" >> Breaking up file into chunks in dir: tmp_seqs\n")

	chunksize = 5000
	paired_fastq = PairedFastqReader(args.fastqa, args.fastqb)
	#chunks = grouper(chunksize, paired_fastq)
	chunks = grouper_witer(chunksize, paired_fastq, None)

	fout1 = open(args.matef, "w")
	fout2 = open(args.mater, "w")
	outfiles = [fout1, fout2]

	# try:
	# 	fout2 = open(args.mater, "w")
	# 	outfiles = [fout1, fout2]

	# except:
	# 	outfiles = [fout1]
	try:
		print("trying: mkdir tmp_seqs")
		os.mkdir("tmp_seqs")

	except FileExistsError:

		print("Earlier attempt at running program failed to delete tmp_seqs directory")
		print("Program will not delete tmp_seqs directory at program end")

	print(" >> Starting fastq filtering on file chunks in tmp_seqs\n")

	counter = 0
	tmp_files1 = []
	tmp_files2 = []
	curr_chunks = []
	all_files = []

	for chunk in chunks:
		tmp1 = "tmp_seqs/tmp.file.%s.m1.fq" % (counter)
		tmp2 = "tmp_seqs/tmp.file.%s.m2.fq" % (counter)
		curr_chunks.append(chunk)
		tmp_files1.append(tmp1)
		tmp_files2.append(tmp2)
		counter += 1

		if counter % (args.cpus*10) == 0:
			#parallel(fq_filt, args.cpus, [ret_dict], curr_chunks, [args.adaptors], list(zip(tmp_files1, tmp_files2)), [args.trim], [args.end], [args.ns])
			out_vals = parallel(fq_filt, args.cpus, [ret_dict], curr_chunks, list(zip(tmp_files1, tmp_files2)), [args.trim], [args.end], [args.ns])
			for val in out_vals:
				all_files.append(val)
			#ret_dict, iter_in, fref, outf_list, trimlen, trimend, ns_allowed
			print(" >> Finished with %s file pieces\n" % counter)


			#replace_files(tmp_files1,outfiles[0],'m1.fq','bbduk.m1.fq')
			#replace_files(tmp_files2,outfiles[1],'m2.fq','.bbduk.m2.fq')
			for tf1, tf2 in zip(tmp_files1, tmp_files2):
				fin1 = open(tf1)
				fin2 = open(tf2)
				print(fin1.read(), end='', file=outfiles[0])
				print(fin2.read(), end='', file=outfiles[1])
				fin1.close()
				fin2.close()
				os.remove(tf1)
				os.remove(tf2)

			
			curr_chunks = []
			tmp_files1 = []
			tmp_files2 = []

	# parallel(fq_filt, args.cpus, [ret_dict], curr_chunks, [args.adaptors], list(zip(tmp_files1, tmp_files2)), [args.trim], [args.end], [args.ns])
	out_vals = parallel(fq_filt, args.cpus, [ret_dict], curr_chunks, list(zip(tmp_files1, tmp_files2)), [args.trim], [args.end], [args.ns])
	for val in out_vals:
		all_files.append(val)
	for tf1, tf2 in zip(tmp_files1, tmp_files2):
		fin1 = open(tf1)
		fin2 = open(tf2)
		print(fin1.read(), end='', file=outfiles[0])
		print(fin2.read(), end='', file=outfiles[1])
		fin1.close()
		fin2.close()
		os.remove(tf1)
		os.remove(tf2)

	#replace_files(tmp_files1,outfiles[0],'m1.fq','bbduk.m1.fq')
	#replace_files(tmp_files2,outfiles[1],'m2.fq','bbduk.m2.fq')

	fout1.close()
	fout2.close()

	# DEST_fn = args.fastqa.replace('.fq', '.bbduk.stats.txt')
	# globber = 'tmp_seqs/tmp.file.*.bbduks.stats.txt'
	# glob_concatenate(globber, DEST_fn)

	# DEST_fn = args.fastqa.replace('.fq', '.bbduk.output.txt')
	# globber = 'tmp_seqs/tmp.file.*.bbduk.output.txt'
	# glob_concatenate(globber, DEST_fn)


	print(" >> Finished filtering sequences... \n")
	print(' ')
	print(start_time)
	print(" >> TIME END: %s\n" % time.strftime("%H:%M:%S %Z"))
	# read_counts = open(args.fastqa.replace('.fq', '.bbduk.counts.txt'), 'w')
	# for key in ret_dict.keys():
	# 	vals = "\t".join([str(x) for x in ret_dict[key]])
	# 	print(vals, file=read_counts)
	#shutil.rmtree("tmp_seqs", ignore_errors=True)
	total,good,bad = 0,0,0
	for key in ret_dict.keys():
		total += ret_dict[key][0]
		good += ret_dict[key][1]
		bad += ret_dict[key][2]
	print(" >> Results from quality filtering:")
	print(" >> total reads: %s\n >> reads removed from filtering: %s\n >> reads remaining: %s" % (total,bad,good))
	shutil.rmtree("tmp_seqs")
	#print(all_files)

	# # should write a small function for piping a list of commands together
	# cat = ['cat', args.fastqa.replace('.fq', '.bbduk.output.txt')]
	# grep = ['grep', 'Input:']
	# cut = ['cut', '-f2']
	# p1 = subprocess.Popen(cat, stdout=subprocess.PIPE)
	# p2 = subprocess.Popen(grep, stdin=p1.stdout, stdout=subprocess.PIPE)
	# p3 = subprocess.Popen(cut, stdin=p2.stdout, stdout=subprocess.PIPE)
	# out,err = p3.communicate()
	# out = str(out).replace('\\n', '').replace("b'", "").replace("'","").split()
	# total_bbduk = 0
	# for x in range(0, len(out), 2):
	# 	total_bbduk += int(out[x])

	# grep[1] = 'Contaminants:'
	# p1 = subprocess.Popen(cat, stdout=subprocess.PIPE)
	# p2 = subprocess.Popen(grep, stdin=p1.stdout, stdout=subprocess.PIPE)
	# p3 = subprocess.Popen(cut, stdin=p2.stdout, stdout=subprocess.PIPE)
	# out,err = p3.communicate()
	# out = str(out).replace('\\n', '').replace("b'", "").replace("'","").split()
	# bad_bbduk = 0
	# for x in range(0, len(out), 3):
	# 	bad_bbduk += int(out[x])

	# print(" >> Results from adaptor filtering using bbduk:")
	# print(" >> total input reads: %s\n >> reads removed from filtering: %s\n >> reads remaining: %s" % (total_bbduk,bad_bbduk,total_bbduk-bad_bbduk))
	return 0





if __name__ == '__main__':
		main()


