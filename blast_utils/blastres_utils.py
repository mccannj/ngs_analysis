#!/usr/bin/env python3

col_lookup = ['qid', 'sid', 'pid', 'alen', 'nmis', 'gopen', 'qstart', 'qend', 'sstart', 'send', 'eval', 'bit']


# create a shelve which indexes the reads in each file that are to be removed
# go through the file once at the end and 



def filter_by(line, indx, oper, other):

	""" Func takes line of file, splits extracts col by indx. Extracted val,
	    operator and other are concat into string. Then the code in the string
	    is eval-ed. Line is printed to new file obj if eval(comp)."""

	line_split = line.rstrip("\n").split("\t")
	xpre = "%s%s%s" % (line_split[indx], oper, other)

	if eval(xpre):
		return 1
	else:
		return 0


def filter_by_print(line, indx, oper, other, fout):

	""" Func takes line of file, splits extracts col by indx. Extracted val,
	    operator and other are concat into string. Then the code in the string
	    is eval-ed. Line is printed to new file obj if eval(comp)."""

	line_split = line.rstrip("\n").split("\t")
	xpre = "%s%s%s" % (line_split[indx], oper, other)

	if eval(xpre):
		print(line, end="", file=fout)
	else:
		return 0

	return 1


def top_hits(in_fname, num_hits, out_fname):

	col_lookup = ['queryid', 'subjid', 'per_ident', 'align_length', 'num_mismatch', 'gopen', 'qstart', 'qend', 'sstart', 'send', 'eval', 'bitscore']
	curr_ctg = ""
	fin = open(in_fname)
	fout = open(out_fname, 'w')
	header = "\t".join(col_lookup)
	#print(header, file=fout)
	count = 0
	for line in fin:

		filt1 = filter_by_print(line, 2, ">=", 80, fout)
		filt2 = filter_by_print(line, 3, ">=", 80, fout)

		if not(filt1 and filt2):
			continue

		line = line.rstrip("\n")
		lsplit = line.split("\t")
		if lsplit[0] != curr_ctg:
			curr_ctg = lsplit[0]
			print(line, file=fout)
			count = 1

		elif count <= num_hits:
			print(line, file=fout)
			count += 1


		elif count == num_hits:
			continue

	fin.close()
	fout.close()

	return 0


def main():
	import argparse as ap

	parser = ap.ArgumentParser()
	parser.add_argument('-i', "--infile", help="infile name", required=True, type=str)
	parser.add_argument('-n', "--numhits", help="number of hits to keep", required=True, type=int)
	parser.add_argument('-o', "--outfile", help="outfile name", required=True, type=str)
	args = parser.parse_args()

	top_hits(args.infile, args.numhits, args.outfile)


if __name__ == '__main__':

	main()

	#in_fname = "/media/melampodium2/NGSdata/q.cup.s.mel_sat.res"
	#out_fname = "/media/melampodium2/NGSdata/q.cup.s.mel_sat.first50.res"

	#top_hits(in_fname, 50, out_fname)
	#fout = open('/home/melampodium2/Documents/human_blast/q.aglpss_5larg_all.s.human_chr17.filtered.alength.gte80.res', 'w')
	#scan = scanner("/home/melampodium2/Documents/human_blast/q.aglpss_5larg_all.s.human_chr17.res", filter_by, 3, ">=", 80, fout)

	#count = 0
	#for val in scan:
	#	count += val
	#print(count)






	