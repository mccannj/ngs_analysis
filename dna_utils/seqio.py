#! /usr/bin/env python3

class Sequence:

	"""Sequence object that can be used for generating sequence objects 
	   from fasta files, things can be added as needed for other applications"""

	def __init__(self, name="", seq="", qual="", seqform=""):
		self.name = name
		self.seq = seq
		self.qual = qual
		self.seqform = seqform
		self.is_revcomp = False

	def check_duplicate(self, other):
		return self.seq == other.seq

	def reverse_complement(self):
		self.seq = reverseCompSequence(self.seq)
		self.qual = reverseSequence(self.qual)
		self.is_revcomp = not(self.is_revcomp)

	def fasta_oneline(self):
		return ">%s\n%s" % (self.name, self.seq)

	def fasta_wrap(self, n=80):
		seq_out = ">%s\n" % self.name
		for x in range(0,len(self.seq),n):
			seq_out += self.seq[x:x+n] + "\n"
		seq_out = seq_out.rstrip('\n')
		return seq_out

	def fastq_out(self):
		return "%s\n%s\n+\n%s" % (self.name,self.seq,self.qual)

	def __str__(self):
		return "name: %s\nseq: %s" % (self.name, self.seq)
	
	def __repr__(self):
		return "name: %s\nseq: %s" % (self.name, self.seq)	

	def __len__(self):
		return len(self.seq)

	def __eq__(self, other):
		return self.name == other.name

rclookup = {
    'a': 't', 't': 'a', 'g': 'c', 'c': 'g',
    'w': 'w', 's': 's', 'm': 'k', 'k': 'm', 'r': 'y', 'y': 'r',
    'b': 'v', 'd': 'h', 'h': 'd', 'v': 'b', 'n': 'n',
    'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G',
    'W': 'W', 'S': 'S', 'M': 'K', 'K': 'M', 'R': 'Y', 'Y': 'R',
    'B': 'V', 'D': 'H', 'H': 'D', 'V': 'B', 'N': 'N', "-": "-"}

def reverseCompSequence(sequence):
    """
    Defines a generic method for reverse complementing a sequence of
    nucleotide codes.  This method fully supports all of the IUPAC
    ambiguity codes.
    """
    tmp = ""

    for base in sequence[::-1]:
        tmp += rclookup[base]

    return tmp
    

def reverseSequence(sequence):
	return sequence[::-1]


def complementSequence(sequence):
	"""returns a complemented sequence"""

	tmp = ""
	
	for base in sequence:
		tmp += rclookup[base]

	return tmp

def trim_read(fq_obj, trim=0):
	if trim > 0:
		fq_obj.seq = fq_obj.seq[trim:]
	elif trim < 0:
		fq_obj.seq = fq_obj.seq[::-1][trim:][::-1]
	return fq_obj


class FastaReader(object):
	"""
	Reader for FASTA files.
	"""
	def __init__(self, file, wholefile=False, keep_linebreaks=False, sequence_class=Sequence):
		"""
		file is a filename or a file-like object.
		If file is a filename, then .gz files are supported.
		If wholefile is True, then it is ok to read the entire file
		into memory. This is faster when there are many newlines in
		the file, but may obviously need a lot of memory.
		keep_linebreaks -- whether to keep the newline characters in the sequence
		"""
		#if isinstance(file, basestring):
		#	file = xopen(file)
		file = open(file)
		self.fp = file
		self.sequence_class = sequence_class
		self.delivers_qualities = False

	def __iter__(self):
		"""
		Read next entry from the file (single entry at a time).

		# TODO this can be quadratic since += is used for the sequence string
		"""
		name = None
		seq = ''
		for line in self.fp:
			# strip() should also take care of DOS line breaks
			line = line.strip()
			if line and line[0] == '>':
				if name is not None:
					assert seq.find('\n') == -1
					yield self.sequence_class(name, seq, None, "fasta")
				name = line[1:]
				seq = ''
			else:
				seq += line
		if name is not None:
			assert seq.find('\n') == -1
			yield self.sequence_class(name, seq, None, "fasta")

	def __enter__(self):
		if self.fp is None:
			raise ValueError("I/O operation on closed FastaReader")
		return self

	def __exit__(self, *args):
		self.fp.close()


class FastqReader(object):
	"""Returns a read-by-read fastQ parser analogous to file.readline()"""
	def __init__(self,filePath,headerSymbols=['@','+']):
		"""Returns a read-by-read fastQ parser analogous to file.readline().
		Exmpl: parser.next()
		-OR-
		Its an iterator so you can do:
		for rec in parser:
			... do something with rec ...
 
		rec is tuple: (seqHeader,seqStr,qualHeader,qualStr)
		"""
		if filePath.endswith('.gz'):
			self._file = gzip.open(filePath)
		else:
			self._file = open(filePath, 'rU')
		self._currentLineNumber = 0
		self._hdSyms = headerSymbols
		 
	def __iter__(self):
		return self
	 
	#def next(self):
	def __next__(self):
		"""Reads in next element, parses, and does minimal verification.
		Returns: tuple: (seqHeader,seqStr,qualHeader,qualStr)"""
		# ++++ Get Next Four Lines ++++
		elemList = []
		for i in range(4):
			line = self._file.readline()
			self._currentLineNumber += 1 ## increment file position
			if line:
				elemList.append(line.strip('\n'))
			else:
				elemList.append(None)
		 
		# ++++ Check Lines For Expected Form ++++
		trues = [bool(x) for x in elemList].count(True)
		nones = elemList.count(None)
		# -- Check for acceptable end of file --
		if nones == 4:
			raise StopIteration
		# -- Make sure we got 4 full lines of data --
		assert trues == 4,\
			   "** ERROR: It looks like I encountered a premature EOF or empty line.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._currentLineNumber)
		# -- Make sure we are in the correct "register" --
		assert elemList[0].startswith(self._hdSyms[0]),\
			   "** ERROR: The 1st line in fastq element does not start with '%s'.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._hdSyms[0],self._currentLineNumber)
		assert elemList[2].startswith(self._hdSyms[1]),\
			   "** ERROR: The 3rd line in fastq element does not start with '%s'.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._hdSyms[1],self._currentLineNumber)
		# -- Make sure the seq line and qual line have equal lengths --
		assert len(elemList[1]) == len(elemList[3]), "** ERROR: The length of Sequence data and Quality data of the last record aren't equal.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._currentLineNumber)
		 
		# ++++ Return fatsQ data as tuple ++++
		#return tuple(elemList)
		#return Fastq(elemList[0], elemList[1], elemList[3])
		return Sequence(name=elemList[0], seq=elemList[1], qual=elemList[3])


class PairedFastqReader(object):
	"""Returns a read-by-read fastQ parser analogous to file.readline()"""
	def __init__(self, filePath1, filePath2, headerSymbols=['@','+']):
		"""Returns a read-by-read fastQ parser analogous to file.readline().
		Exmpl: parser.next()
		-OR-
		Its an iterator so you can do:
		for rec in parser:
			... do something with rec ...
 
		rec is tuple: (seqHeader,seqStr,qualHeader,qualStr)
		"""
		if filePath1.endswith('.gz'):
			self._file1 = gzip.open(filePath1)
		else:
			self._file1 = open(filePath1, 'rU')

		if filePath2.endswith('.gz'):
			self._file2 = gzip.open(filePath2)
		else:
			self._file2 = open(filePath2, 'rU')

		self._currentLineNumber = 0
		self._hdSyms = headerSymbols
		 
	def __iter__(self):
		return self
	 
	#def next(self):
	def __next__(self):
		"""Reads in next element, parses, and does minimal verification.
		Returns: tuple: (seqHeader,seqStr,qualHeader,qualStr)"""
		# ++++ Get Next Four Lines ++++
		elemList = []
		for i in range(4):
			line1 = self._file1.readline()
			line2 = self._file2.readline()
			self._currentLineNumber += 1 ## increment file position
			if line1 and line2:
				elemList.append(line1.strip('\n'))
				elemList.append(line2.strip('\n'))
			else:
				elemList.append(None)
		 
		# ++++ Check Lines For Expected Form ++++
		trues = [bool(x) for x in elemList].count(True)
		nones = elemList.count(None)
		# -- Check for acceptable end of file --
		if nones == 4:

			raise StopIteration
		# -- Make sure we got 4 full lines of data --
		assert trues == 8,\
			   "** ERROR: It looks like I encountered a premature EOF or empty line.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._currentLineNumber)
		# -- Make sure we are in the correct "register" --
		assert elemList[0].startswith(self._hdSyms[0]),\
			   "** ERROR: The 1st line in fastq element does not start with '%s'.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._hdSyms[0],self._currentLineNumber)
		assert elemList[1].startswith(self._hdSyms[0]),\
			   "** ERROR: The 1st line in fastq element does not start with '%s'.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._hdSyms[0],self._currentLineNumber)
		assert elemList[4].startswith(self._hdSyms[1]),\
			   "** ERROR: The 3rd line in fastq element does not start with '%s'.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._hdSyms[1],self._currentLineNumber)
		assert elemList[5].startswith(self._hdSyms[1]),\
			   "** ERROR: The 3rd line in fastq element does not start with '%s'.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._hdSyms[1],self._currentLineNumber)
		# -- Make sure the seq line and qual line have equal lengths --
		assert len(elemList[2]) == len(elemList[6]), "** ERROR: The length of Sequence data and Quality data of the last record aren't equal.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._currentLineNumber)
		assert len(elemList[3]) == len(elemList[7]), "** ERROR: The length of Sequence data and Quality data of the last record aren't equal.\n\
			   Please check FastQ file near line number %s (plus or minus ~4 lines) and try again**" % (self._currentLineNumber)
		 
		# ++++ Return fatsQ data as tuple ++++
		#return tuple(elemList)
		#return Fastq(elemList[0], elemList[1], elemList[3])
		return (Sequence(name=elemList[0], seq=elemList[2], qual=elemList[6]),
				Sequence(name=elemList[1], seq=elemList[3], qual=elemList[7]))

	def __exit__(self, *args):
		self._file1.close()
		self._file2.close()

def split_fasta(multifasta_fn, outf_prefix, chunksize=500):

	#infile_handle = open(multifasta_fn)

	seq_counter = 0
	output_file_number = 0
	#fasta_handle = parseFasta(infile_handle)
	fasta_handle = (x for x in FastaReader(multifasta_fn))
	out_hands = []

	while True:

		try:
			seq_obj = next(fasta_handle)

			if seq_counter % chunksize == 0: 
				fasta_out = open("%s.%s.fas" % (outf_prefix, output_file_number), 'w')
				out_hands.append("%s.%s.fas" % (outf_prefix, output_file_number))
				output_file_number += 1

			
			if seq_obj is None:
				#print("Split original fasta into %s multi-fasta files" % output_file_number)
				break
			#print(fasta_handle, seq_obj, sep="\n")
			seq_counter += 1
			print(seq_obj.fasta_wrap(80), file=fasta_out)

		except StopIteration:
			
			print("Split original fasta into %s multi-fasta files" % output_file_number)
			break

	return out_hands

def split_fastq(fastq_fn, outf_prefix, chunksize=500):

	#infile_handle = open(multifasta_fn)

	seq_counter = 0
	output_file_number = 0
	#fasta_handle = parseFasta(infile_handle)
	fastq_handle = FastqReader(fastq_fn)
	out_hands = []
	dreplace = {'@':'', ':':'_', '#':'_', '/':'_'}

	while True:

		try:
			seq_obj = next(fastq_handle)

			#for key in dreplace.keys():
			#	seq_obj.name = seq_obj.name.replace(key, dreplace[key])

			if seq_counter % chunksize == 0: 
				fastq_outfile = open("%s.%s.fq" % (outf_prefix, output_file_number), 'w')
				out_hands.append("%s.%s.fq" % (outf_prefix, output_file_number))
				output_file_number += 1

			
			if seq_obj is None:
				#print("Split original fasta into %s multi-fasta files" % output_file_number)
				break
			#print(fasta_handle, seq_obj, sep="\n")
			seq_counter += 1
			print(seq_obj.fastq_out(), file=fastq_outfile)

		except StopIteration:
			
			print("Split original fastq into %s fastq files" % output_file_number)
			break

	return out_hands

def split_paired_fastq(fq_list, outf_prefix, chunksize=500):

	#infile_handle = open(multifasta_fn)

	seq_counter = 0
	output_file_number = 0
	#fasta_handle = parseFasta(infile_handle)
	fastq_handle = PairedFastqReader(fq_list[0], fq_list[1])
	out_hands = []
	dreplace = {'@':'', ':':'_', '#':'_', '/':'_'}

	while True:

		try:
			seq1, seq2 = next(fastq_handle)

			#for key in dreplace.keys():
			#	seq_obj.name = seq_obj.name.replace(key, dreplace[key])

			if seq_counter % chunksize == 0:
				fn1 = "%s.%s.%s.fq" % (outf_prefix, output_file_number, 'm1')
				fn2 = "%s.%s.%s.fq" % (outf_prefix, output_file_number, 'm2')
				fastq_outfile1 = open(fn1, 'w')
				fastq_outfile2 = open(fn2, 'w')
				out_hands.append([fn1,fn2])
				#out_hands.append(fn2)
				output_file_number += 1

			
			if seq1 is None:
				#print("Split original fasta into %s multi-fasta files" % output_file_number)
				break
			#print(fasta_handle, seq_obj, sep="\n")
			seq_counter += 2
			print(seq1.fastq_out(), file=fastq_outfile1)
			print(seq2.fastq_out(), file=fastq_outfile2)

		except StopIteration:
			
			print("Split original paired fastq files into %s fastq files" % output_file_number)
			break

	return out_hands

def split_paired_fastq_single(fq_list, outf_prefix, chunksize=500):

	#infile_handle = open(multifasta_fn)

	seq_counter = 0
	output_file_number = 0
	#fasta_handle = parseFasta(infile_handle)
	fastq_handle = PairedFastqReader(fq_list[0], fq_list[1])
	out_hands = []
	dreplace = {'@':'', ':':'_', '#':'_', '/':'_'}

	while True:

		try:
			seq1, seq2 = next(fastq_handle)

			#for key in dreplace.keys():
			#	seq_obj.name = seq_obj.name.replace(key, dreplace[key])

			if seq_counter % chunksize == 0:
				fn1 = "%s.%s.%s.fq" % (outf_prefix, output_file_number, 'm1')
				#fn2 = "%s.%s.%s.fq" % (outf_prefix, output_file_number, 'm2')
				fastq_outfile1 = open(fn1, 'w')
				#fastq_outfile2 = open(fn2, 'w')
				out_hands.append(fn1)
				#out_hands.append(fn2)
				output_file_number += 1

			if seq1 is None:
				#print("Split original fasta into %s multi-fasta files" % output_file_number)
				break
			#print(fasta_handle, seq_obj, sep="\n")
			seq_counter += 2
			print(seq1.fastq_out(), file=fastq_outfile1)
			print(seq2.fastq_out(), file=fastq_outfile1)

		except StopIteration:
			
			print("Split original paired fastq files into %s fastq files" % output_file_number)
			break

	return out_hands


if __name__ == "__main__":
	
	fastq1_fn = '/media/mccannj/fsjamie/22773_first.m1.4k.fq'
	fastq2_fn = '/media/mccannj/fsjamie/22773_first.m2.4k.fq'
	fasta = '/home/mccannj/Dropbox/test.in.fasta'

	#count = 0
	#for m1,m2 in PairedFastqReader(fastq1_fn, fastq2_fn):
	#	count += 1
	#	print(m1.fastq_out())
	#	count += 1
	#	print(m2.fastq_out())
	#	print(count)
	
	split_paired_fastq(fastq1_fn, fastq2_fn, "q.seq")
	#split_fastq(fastq_fn, 'q.seq', chunksize=250)
	#split_fasta(fasta, 'fas.seq', chunksize=250)