#! /usr/bin/env python3

import os

from dna_utils.seqio import PairedFastqReader, trim_read
from other_utils.misc_utils import grouper
import shutil


def interlacer(iter_in, prefix, trim, outfn, count):

	fout = open(outfn, 'w')

	for seq1,seq2 in iter_in:
		seq1.name = "%s_%s_f" % (prefix, count)
		seq2.name = "%s_%s_r" % (prefix, count)

		seq1 = trim_read(seq1, trim)
		seq2 = trim_read(seq2, trim)

		print(seq1.fasta_wrap(80), file=fout)
		print(seq2.fasta_wrap(80), file=fout)
		count += 1


def main():

	import argparse as ap
	from other_utils.partools import parallel
	import multiprocessing as mp
	import time
	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-a', "--fastqa", help="fastq first mate infile to be analyzed",
						required=True, type=str)
	parser.add_argument('-b', "--fastqb", help="fastq second mate infile to be analyzed",
						required=True, type=str)
	parser.add_argument('-o', "--outfile", help="outfile for storage of fasta sequences",
						required=True, type=str)
	parser.add_argument('-p', "--prefix", help="prefix for renaming sequences",
						required=True, type=str)
	parser.add_argument('-t', "--trim", help="integer for how many seqs to trim",
						required=False, type=int, default=0)
	parser.add_argument('-c', "--cpus", help="number of cpus to use, default: ALL",
						required=False, type=int, default=mp.cpu_count())

	args = parser.parse_args()

	start_time = "\n >> TIME START: %s" % time.strftime("%H:%M:%S %Z\n")
	
	print(" >> Number of CPUs available: %s" % args.cpus)
	print(" >> Number of CPUs to be used: %s\n" % args.cpus)
	
	chunksize = 5000
	paired_fastq = PairedFastqReader(args.fastqa, args.fastqb)
	chunks = grouper(chunksize, paired_fastq)

	try:
		print("trying: mkdir tmp_seqs")
		os.mkdir("tmp_seqs")

	except FileExistsError:

		print("Earlier attempt at running program failed to delete tmp_seqs directory")
		print("Program will delete tmp_seqs directory at program end")

	print(" >> Starting fasta interlacing in tmp_seqs\n")

	counter = 0
	chunk_counter = []
	tmp_files = []
	curr_chunks = []

	for chunk in chunks:
		tmp = "tmp_seqs/tmp.file.%s.fas" % (counter)
		curr_chunks.append(chunk)
		tmp_files.append(tmp)
		chunk_counter.append(chunksize*counter)
		counter += 1
		
		#print(chunk_counter)

		if counter % (args.cpus*10) == 0:
			parallel(interlacer, args.cpus, curr_chunks, [args.prefix], [args.trim], tmp_files, chunk_counter)
			print(" >> Finished with %s file pieces\n" % counter)
			curr_chunks = []
			chunk_counter = []
			tmp_files = []

	parallel(interlacer, args.cpus, curr_chunks, [args.prefix], [args.trim], tmp_files, chunk_counter)

	DEST = open(args.outfile, 'w')
	for x in range(0, counter):
		fin = open("tmp_seqs/tmp.file.%s.fas" % (x))
		print(fin.read(), end='', file=DEST)
		fin.close()
		os.remove("tmp_seqs/tmp.file.%s.fas" % (x))
	DEST.close()
        shutil.rmtree("tmp_seqs")


	print(" >> Finished interlacing and converting sequences to fasta... \n")
	print(' ')
	print(start_time)
	print(" >> TIME END: %s\n" % time.strftime("%H:%M:%S %Z"))
	return 0

if __name__ == '__main__':
	main()
