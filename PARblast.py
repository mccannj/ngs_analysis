#! /usr/bin/env python3

import os
from glob import iglob

from dna_utils import seqio
import time

# can add other opt parameters for e-value and output format
def blastn(inf, db, outf, evalue):
	
	outfmt = "6 qseqid sseqid staxids sscinames sskingdoms stitle sblastnames pident length mismatch gapopen qstart qend sstart send evalue bitscore"
	print("started: %s; process: blastn; query %s; numthreads=1" % (time.strftime("%H:%M:%S %Z"), inf))
	os.system("blastn -query %s -db %s -out %s -outfmt '%s' -num_threads 1 -evalue %s" % (inf, db, outf, outfmt, evalue))
	print("finished: %s; process: blastn; query %s; out: %s" % (time.strftime("%H:%M:%S %Z"), inf, outf))
	#os.remove(inf)
	return 0


def blastx(inf, db, outf, evalue):

	outfmt = "6 qseqid sseqid staxids sscinames sskingdoms stitle sblastnames pident length mismatch gapopen qstart qend sstart send evalue bitscore"
	print("started: %s; blastx -query %s -db %s -out %s -outfmt '%s' -num_threads 1" % (time.strftime("%H:%M:%S %Z"), inf, db, outf, outfmt))
	os.system("blastx -query %s -db %s -out %s -outfmt '%s' -num_threads 1 -evalue %s" % (inf, db, outf, outfmt, evalue))
	print("finished: %s; blastx -query %s -db %s -out %s -outfmt '%s' -num_threads 1" % (time.strftime("%H:%M:%S %Z"), inf, db, outf, outfmt))
	
	#os.remove(inf)

	return 0


def blastall(func, inf, db, outf, evalue):
		cmd = "blastall -p %s -i %s -d %s -o %s -e %s -m 8 -v 80000 -b 80000 -F F" % (func, inf, db, outf, evalue)
		print(cmd)
		os.system(cmd)
		return 0


def main():
	
	from other_utils.partools import parallel
	import argparse as ap
	from multiprocessing import cpu_count

	parser = ap.ArgumentParser()

	parser.add_argument('-i', "--infile", help="fasta format sequences to be blasted",
						required=True, type=str)
	parser.add_argument('-f', "--function", help="blast type: blastn, blastx (blastall must be installed!)",
						required=False, type=str, default='blastn')
	parser.add_argument('-d', "--databases", help="path and prefix of databases (space delimited) to be blasted", nargs="+",
						required=True, type=str)
	parser.add_argument('-c', "--chunksize", help="maximum should be no higher than num_seqs/ncpus, default: 5000",
						required=False, type=int, default=5000)
	parser.add_argument('-n', "--ncpus", help="number of cpus to use, default: ALL",
						required=False, type=int, default=cpu_count())
	parser.add_argument('-e', '--evalue', help='max evalue for blast hits to database, default: 1e-10',
						required=False, type=str, default="1e-10")

	args = parser.parse_args()

	fun_name_dict = {'blastx':blastx, 'blastn':blastn}
	if args.function not in fun_name_dict:
		print("-f argument must equal 'blastx' or 'blastn'")
		return 1

	f = fun_name_dict[args.function]
	
	# preparing args for parallelization
	print("splitting fasta into pieces for parallel blast")
	ins = seqio.split_fasta(args.infile, 'q.seq.chunk', chunksize=args.chunksize)
	ins*=len(args.databases)

	dbs = []
	for db in args.databases:
		dbs += [db]*(len(ins)//len(args.databases))
		#dbs += [db]*(len(ins))
	
	alt_idxs = list(range(0,len(ins)//len(args.databases)))*len(args.databases)
	outs = ['q.seq.chunk.%s.s.%s.blast.res' % (alt_idxs[x], os.path.split(dbs[x])[-1]) for x in range(0,len(alt_idxs))]

	###########

	for x,y,z in zip(ins,outs,dbs):
		print(x,y,z)
	print(len(ins), len(outs), len(dbs))
	parallel(f, args.ncpus, ins, dbs, outs, [args.evalue])
	#parallel(blastall, args.ncpus, [args.function], ins, dbs, outs, [args.evalue])



	for db in args.databases:
		db_name = os.path.split(db)[-1]
		#print(db_name, args.infile, args.infile.split(".")[-1])
		DEST = open(args.infile.replace("." + args.infile.split('.')[-1], '.%s.blast.res' % db_name), 'w')
		#print(args.infile.replace("." + args.infile.split('.')[-1], '%s.blast.res' % db_name))
		
		for x in range(0, len(outs)//len(args.databases)):
			filename = 'q.seq.chunk.%s.s.%s.blast.res' % (x, db_name)
			fin = open(filename)
			print(fin.read(), end='', file=DEST)
			fin.close()
			os.remove(filename)
		DEST.close()

	for filename in iglob('q.seq.chunk.*'):
		os.remove(filename)

	return 0

if __name__ == '__main__':
	main()
