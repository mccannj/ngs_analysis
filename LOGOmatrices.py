#! /usr/bin/env python3

from dna_utils.seqio import FastaReader

def matrix_generator():
	
	# matrix A, C, G, T
	matrix = [[],[],[],[]]
	return matrix


def main():

	import argparse as ap

	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-a', "--alignment", help="alignment to be analyzed",
						required=True, type=str)
	parser.add_argument('-k', "--kmerfile", help="file where kmers and freqs are located",
						required=True, type=str)
	
	args = parser.parse_args()

	alignment = FastaReader(args.infile)
	


	return 0

if __name__ == '__main__':
	main()