#! /usr/bin/env python3


from dna_utils.seqio import FastaReader

def filter_by_id(fasta, blast_ids, fmatch, funmatch):

	#fasta_in = open(in_fasta)

	#fasta = FastaReader(fasta_in)
	blast = open(blast_ids)

	fout_match = open(fmatch, "w")
	fout_unmatch = open(funmatch, "w")

	contam_dict = {line.rstrip('\n'):0 for line in blast}
	count_good = 0
	count_bad = 0

	fasta_iter = (x for x in fasta)

	while True:

		try:

			read1 = next(fasta_iter)
			read2 = next(fasta_iter)

			if not(read1.name in contam_dict or read2.name in contam_dict):

				count_good += 2
				print(read1.fasta_wrap(80), file=fout_unmatch)
				print(read2.fasta_wrap(80), file=fout_unmatch)

			else:
				print(read1.fasta_wrap(80), file=fout_match)
				print(read2.fasta_wrap(80), file=fout_match)
				count_bad += 2

		except StopIteration:
			break

	print("# of unmatched seqs to blast output %s" % (count_good))
	print("# of matched seqs to blast output %s" % (count_bad))
	print("finished")

	return 0


def main():

	import argparse as ap
	import time
	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-f', "--fasta", help="fasta infile to be analyzed",
						required=True, type=str)
	parser.add_argument('-b', "--blastids", help='line-delimited fasta ids from blast for removal',
						required=True, type=str)
	parser.add_argument('-m', "--matched", help="outfile for storage of matched fasta sequences",
						required=True, type=str)
	parser.add_argument('-u', "--unmatched", help="outfile for storage of unmatched fasta sequences",
						required=True, type=str)


	args = parser.parse_args()

	start_time = "\n >> TIME START: %s" % time.strftime("%H:%M:%S %Z\n")
	
	fasta = FastaReader(args.fasta)

	filter_by_id(fasta, args.blastids, args.matched, args.unmatched)




	print(" >> Finished filtering sequences to fasta... \n")
	print(' ')
	print(start_time)
	print(" >> TIME END: %s\n" % time.strftime("%H:%M:%S %Z"))

	return 0

if __name__ == '__main__':
	main()