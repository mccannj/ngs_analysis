#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  KMERfrags.py
#  


def get_kmers(kmers_fn, min_freq_kmers):
	
	"""creates dict of kmers from file"""
	
	f = open(kmers_fn)
	kmers = {}

	for line in f:
		# line format
		# AAGCTGTCAG	10049	0.004536273623
		line = line.rstrip("\n").split("\t")
		kmer = line[0]
		freq = float(line[2])
	
		if freq >= min_freq_kmers: kmers[kmer] = freq
	
	f.close()
	return kmers


def extend_kmers(kmer, reverse=False):
	
	""" returns a new kmer dict with bases added
	    to the 5' or 3' position of the original kmer """
	
	bases = ["A","G","T","C"]
	
	if reverse: 
		new_kmers = {base + kmer[:-1]: 0 for base in bases}
	else:
		new_kmers = {kmer[1:] + base: 0 for base in bases}
	
	return new_kmers


def best_kmers(new_kmers_dict, org_kmers_dict, reverse=False):
	
	""" returns a dict with best freq, base and kmer """
	
	base_fun = lambda x, kmer : kmer[0] if x else kmer[-1]
	best = {"freq":0, "base":"N", "kmer":"N"}

	for key in new_kmers_dict.keys():
		
		if org_kmers_dict.get(key,0) > best["freq"]:
			best["freq"] = org_kmers_dict[key]
			best["base"] = base_fun(reverse, key)
			best["kmer"] = key
	
	return best


def sum_kmers(kmers_dict, kmers_fn, min_seed_freq, coefficient, min_freq_kmers):
	
	sum_freqs = {}
	# sorts kmers by descending frequency
	keys_sort = sorted(kmers_dict.keys(), key=lambda x: kmers_dict[x], reverse=True)
	
	# output files
	out_kmers = open("%s.%s_%s_rek_kmers_mine" % (kmers_fn, min_seed_freq, coefficient), "w")
	out_fasta = open("%s.%s_%s_rek_fasta_mine" % (kmers_fn, min_seed_freq, coefficient), "w")
	
	# initialize counter for groups, incremented in body
	g=0
	for kmer in keys_sort:
		
		try: # if kmer has been removed from dict a KeyError is raised
			if kmers_dict[kmer] < min_seed_freq: break
		except KeyError: # this just tells to return to top of for loop
			continue
		
		g+=1
		group = "group_%s" % g
		sum_freqs[group] = kmers_dict[kmer] # add freq for current kmer to group key
		
		# initialize contig as kmer
		contig = kmer
		cutoff = kmers_dict[kmer] * coefficient # sets the cutoff values for adding new kmer
		
		print(group, file=out_kmers)
		print(">%s\t%s" % (kmers_dict[kmer], kmer), file=out_kmers)
		
		# have to remove seed from dict so it is not counted twice
		del kmers_dict[kmer]

		# the following section contains 2 nearly identical subsects of code
		# find a way to abstract this out into one function with a switch
		
		seed = kmer # start seed always at kmer in for loop
		while True:
			# extend in 3' direction
			new_kmers = extend_kmers(seed)
			best = best_kmers(new_kmers, kmers_dict)
			
			if best["freq"] > cutoff:
				seed = best["kmer"]
				contig = contig + best["base"]
				sum_freqs[group] += kmers_dict[seed]
				print(">%s\t%s" % (kmers_dict[seed], seed), file=out_kmers)
				# have to remove seed from dict so it is not counted twice
				del kmers_dict[seed]
				
			else:
				break

		seed = kmer
		while True:
			# extend in 5' direction
			new_kmers = extend_kmers(seed, reverse=True)
			best = best_kmers(new_kmers, kmers_dict, reverse=True)
		
			if best["freq"] > cutoff:
				seed = best["kmer"]
				contig = best["base"] + contig
				sum_freqs[group] += kmers_dict[seed]
				print(">%s\t%s" % (kmers_dict[seed], seed), file=out_kmers)
		 		# have to remove seed from dict so it is not counted twice
				del kmers_dict[seed]
			
			else:
				break
		
		print(">%s (%s)\n%s" % (group, sum_freqs[group], contig), file=out_fasta)
		
	out_kmers.close()
	out_fasta.close()
	return 0


def main():
	import argparse as ap
	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-i', "--infile", help="infile to be analyzed",
						required=True, type=str)
	parser.add_argument('-s', "--seedfreq", help="min seed kmer freq to use from kmer file",
						required=True, type=float)
	parser.add_argument('-e', "--extfreq", help="min kmer extension freq expressed as proportion\
						of seed kmer frequency", required=True, type=float)

	args = parser.parse_args()

	min_freq_kmers = args.seedfreq*args.extfreq

	kmers = get_kmers(args.infile, min_freq_kmers)
	sum_kmers(kmers, args.infile, args.seedfreq, args.extfreq, min_freq_kmers)
	return 0

if __name__ == "__main__":
	main()

