#/usr/bin/env python3

from dna_utils.seqio import FastaReader


def parseInfile(arg_f):

	fin = open(arg_f, "r")
	record_count = 0
	prefix_dict = {}

	for line in fin:

		if line.startswith(">"):

			line = line[1:].rstrip("\n").split("/")
			dir_name = line[0] + "/seqClust/clustering/clusters"
			prefix = line[1]
			prefix_dict[prefix] = []
			record_count += 1

		else:

			l = line.rstrip("\n")
			dir_num = "%s/dir_CL%04d" % (dir_name, int(l))
			prefix_dict[prefix].append(dir_num)

	return prefix_dict


def getContigs(key, seq_gen_obj):

	contigs_list = []

	for x in range(0,5):

		try:
			record = next(seq_gen_obj)
			name = record.name.split()
			record.name = "%s_%s %s" % (name[0], key, name[1])
			contigs_list.append(record)

		except StopIteration:
			return contigs_list

	return contigs_list


def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-f', type=str)
	parser.add_argument('-o', type=str)
	args = parser.parse_args()

	prefix_dict = parseInfile(args.f)
	fout = open(args.o, 'w')
	keys = [x for x in prefix_dict.keys()]

	for key in sorted(keys):
		fn_prefix = "5_largest_contigs_"
		fn_suffix = ".fas"

		for d in prefix_dict[key]:
			val = d[-4:].lstrip("0")
			contigs_in = open("/media/melampodium2/NGSdata/%s/contigs_CL%s.minRD5_sort-GR" % (d, val))
			for contig in FastaReader(contigs_in):
				contig.name = key + "_" + contig.name.split(" ")[0] 
				print(contig.fasta_wrap(80), end="", file=fout)
			contigs_in.close()

	return 0

if __name__ == '__main__':
	main()
