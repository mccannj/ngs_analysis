#! /usr/bin/env python3

"""
This is a program which works in concert with blast results to orient the
input reads all in the same direction for further kmer analysis. This program
is highly dependent on the results of blast. If you get poor blast results then
the orientation of the reads will also be poor.

"""

import sys
sys.path.append("/home/mccannj/Python/read_processing")

from dna_utils.seqio import FastaReader, reverseCompSequence

def main():
	import argparse as ap
	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-i', "--infile", help="reads to be oriented",
						required=True, type=str)
	parser.add_argument('-c', "--corient", help="read orientation file",
						required=True, type=str)
	parser.add_argument('-r', "--rorient", help="contig orientation file",
						required=True, type=str)
	
	args = parser.parse_args()

	fasta = FastaReader(args.infile)

	rorient_dict = {}
	corient_dict = {}

	file_rorient = open(args.rorient)
	file_corient = open(args.corient)

	for line in file_rorient:
		line = line.rstrip("\n").split("\t")
		rorient_dict[line[0].rstrip()] = (line[1].rstrip(), line[2])

	for line in file_corient:
		line = line.rstrip("\n").split("\t")
		corient_dict[line[0].rstrip()] = line[1]

	fout = open(args.infile.replace("fas", "oriented.fas"), 'w')

	for aseq in fasta:

		try:
			read = rorient_dict[aseq.name]
			
			read_ctg = read[0]
			read_orient = read[1]
			ctg_orient = corient_dict[read_ctg]

			if read_orient == ctg_orient:
				print(aseq.fasta_wrap(80), file=fout)
			else:
				rc_seq = reverseCompSequence(aseq.seq)
				aseq.seq = rc_seq
				print(aseq.fasta_wrap(80), file=fout)

		except KeyError:
			continue

	print("Finished orienting reads >> %s" % args.infile.replace("fas", "oriented.fas"))
	fout.close()




if __name__ == '__main__':
	main()