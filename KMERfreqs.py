#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  KMERfreqs.py
#  
#  Copyright 2014 Jamie <jamie.mccann@univie.ac.at>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# Calculates k-mer frequencies for input sequence(s)
# Saves kmers and frequencies to an output file for further processing

from dna_utils.seqio import FastaReader


def main():
	
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', "--infile", help="fasta file name", required=False, type=str)
	parser.add_argument('-k', "--kmer_length", help="length of kmers to be generated", required=False, type=int)
	parser.add_argument('-o', "--outfile", help="name of output file", required=False, type=str)
	args = parser.parse_args()
	
	# initialize args
	
	if not args.infile:
		print("\nThe following argument must be specified --> -i or --infile")
		print("\nPlease specify a file name where sequences are located")
		print("USAGE: ./KMERFreq.py -i infile.fas -k # -o outfile.kmer")
		print("\nPROGRAM EXIT!\n")
		return 1
	else:
		fn = args.infile

	if not args.kmer_length:
		kmer_len = 10
		print("\nKmer length was not set by the user")
		print("Setting default kmer length to >> 10 <<\n")
	else:
		kmer_len = args.kmer_length
		print("\nKmer length set to >> %s <<\n" % kmer_len)
		
	if not args.outfile:
		out_fn = fn.rstrip(".fasta") + ".kmer"
		print("Output file name was not set by the user")
		print("Setting default output file name to >> %s <<\n" % out_fn)
	else:
		out_fn = args.outfile
		print("Output file name set to >> %s <<\n" % out_fn)
	
	kmers = {}
	#fasta_open = open(fn, "rU")
	fasta_file = (x for x in FastaReader(fn))
	#fasta = parseFasta(fasta_open)
	
	while True:
		
		try:
			
			seq_obj = next(fasta_file) # sub for fasta.__next__()
			
			# end (len(sequence)-kmer_len) bases before end of seq to prevent kmers < len(kmer_len)
			start, end = 0, (len(seq_obj)-kmer_len)
			sequence = seq_obj.seq
			
			for f in range(start,end):
				
				#create kmer from sequence and make all uppercase
				kmer = sequence[f:f+kmer_len].upper()
				
				# check if kmer is already in kmers dict and if ""
				if kmer in kmers and "N" not in kmer:
					kmers[kmer] += 1
				
				elif kmer not in kmers and "N" not in kmer:
					kmers[kmer] = 1
					
				else:
					print("Excluding the following kmer: %s" % kmer)
					print("Kmer comes from sequence: %s" % seq_obj.name)
					
		except StopIteration:
			
			# sort kmers by frequency and write out kmer seq and freq to file in tdt format
			keys_sort = sorted(kmers.keys(), key=lambda x: kmers[x], reverse=True)
			kmers_unique = len(kmers.keys())
			kmers_total = sum(kmers.values())
			
			outfile = open(out_fn, "w")
			print("OUTPUT TO FILE\nFilename = %s\n" % outfile.name)
			
			for key in keys_sort:
				print(key, kmers[key], kmers[key]/kmers_total, sep="\t", file=outfile)

			print('UNIQUE KMERS: %s\nTOTAL KMERS: %s\n' % (kmers_unique, kmers_total))
			break
			

if __name__ == "__main__":
	main()
		
