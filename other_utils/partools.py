#! /usr/bin/env python3
import multiprocessing
import os
import time
import random

# nice function that takes normal function and sends the function with its args down pipe
def spawn(func):
	""" returns a func of the form f(x,y);
	    takes two args, an end of a pipe and 
	    a set of arguments """

	def fun(pipe,x):
		""" sends function ready to be called across pipe """
		pipe.send(func(x))
		pipe.close()

	return fun


def mp_parmap(cmd, ncpus, arg_iter):

	max_proc = ncpus

	pipes = []
	procs = []
	retvals = {}

	counter = 0

	for args in arg_iter:
		counter += 1
		pipes.append(multiprocessing.Pipe())
		# Does it matter if pipe[-1][1] or pipe[-1][0] (which end of the pipe)? No, tested.
		# When "recv" is called, needs to be called from other end not spec'd here.
		procs.append(multiprocessing.Process(target=spawn(cmd), args=(pipes[-1][0], args), name=str(counter)))
		
		# needs to be -1 because append is to end of list, 0 would not work
		curr_proc = procs[-1]

		# counts alive processes
		while True:
			running = 0

			for proc in procs:
				if proc.is_alive(): running += 1

			if running < max_proc:
				break
			else:
				time.sleep(0.1)

		curr_proc.start()
		print("process %s %s %s has been started" % (cmd.__name__, curr_proc.pid, curr_proc.name))
		

		# Join finished processes, append results to retvals and close pipe ends!
		for proc, pipe in zip(procs, pipes):
			
			if proc.pid and not proc.exitcode and not proc.is_alive() and (proc.name not in retvals):
				proc.join()
				#print("process %s %s %s has completed" % (cmd.__name__, curr_proc.pid, curr_proc.name))
				retvals[int(proc.name)] = pipe[1].recv()
				pipe[0].close()
				pipe[1].close()
				pipes.remove(pipe)
				procs.remove(proc)

	# There will be a point when no more proc are added to procs
	# Collect the remaining proc; join, append, close!
	for proc in procs:
		proc.join()
		

	for proc, pipe in zip(procs,pipes):
		if proc.pid and not proc.exitcode and not proc.is_alive() and (proc.name not in retvals):
			retvals[int(proc.name)] = pipe[1].recv()
			pipe[0].close()
			pipe[1].close()

	# make list of retvals in correct order
	retvals = [retvals[key] for key in sorted(retvals.keys())]
	return retvals


def parallel(cmd, ncpus, *args):

	# *args is a tuple of args of indeterminate length

	len_lists = [len(li) for li in args]
	args_lists = [list(x) for x in args]
	high = max(len_lists)

	if len(set(len_lists)) == 1:
		# len of lists are all the same
		print("args are correct length!")
		

	elif set(len_lists) == set([1,high]):
		# expanding lists of len = 1

		for i in range(len(args_lists)):
			if len(args_lists[i]) == 1:
				args_lists[i] *= high

		args = [tuple(x) for x in args_lists]

	else:
		print("Args not of correct length!")
		# perhaps here have to raise ValueError or something
		return 1

	args_tups = zip(*args)

	def cmd_star(args):
		return cmd(*args)

	return mp_parmap(cmd_star, ncpus, args_tups)

# a sample function for testing
def iter_sample_fast(iterable, samplesize):
	results = []
	iterator = iter(iterable)
	# Fill in the first samplesize elements:
	# generates a list of items of desired sample size, shuffles,
	# then randomly replaces these items with other items from the iterable
	# Fill in the first samplesize elements:
	try:
		for x in range(samplesize):
			results.append(next(iterator))

	except StopIteration:
		raise ValueError("Sample larger than population.")

	random.shuffle(results)  # Randomize their positions

	for i, v in enumerate(iterator, samplesize):
		r = random.randint(0, i)
		if r < samplesize:
			results[r] = v  # at a decreasing rate, replace random items
	counter = 0
	for val in results:
		counter += val
	return counter


if __name__ == "__main__":
	parallel(iter_sample_fast, 8, [10], [5,6,7,8,9], [10,20,30,40,50])
	#y = parallel(iter_sample_fast, 8, [range(0,10000) for x in range(0,40)], [1000 for x in range(0,40)])
	#print(y, len(y))
