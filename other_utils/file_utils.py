#!/usr/bin/env python3
import os
from glob import iglob

import sys

sys.path.append("/Users/jlmccann0908/Dropbox/python_workspace/")
sys.path.append("/home/melampodium2/Dropbox/python_workspace/")
sys.path.append("/home/mccannj/Dropbox/python_workspace")
sys.path.append("/home/jamie/Dropbox/python_workspace")


def scanner(name, func, *args):

	fin = open(name)

	for line in fin:
		yield func(line, *args)

	fin.close()
	return 0


def replace_files(file_list, destination, id1, id2):

	for fn in file_list:
		fin = open(fn.replace(id1,id2))
		print(fin.read(), end="", file=destination)
		fin.close()
		os.remove(fn)
		os.remove(fn.replace(id1,id2))
	return 0


def glob_concatenate(glob_string, dest_fn):

	DEST = open(dest_fn, 'w')
	for filename in iglob(glob_string):
		fin = open(filename)
		print(fin.read(), end='', file=DEST)
		fin.close()
		os.remove(filename)
	DEST.close()
	return 0


if __name__ == '__main__':

	# chunks = grouper(5000, PairedFastqReader('/media/mccannj/fsjamie/22773_first.m1.4k.fq', '/media/mccannj/fsjamie/22773_first.m2.4k.fq'))

	# for val in chunks:
	# 	print(val)
	pass