#!/usr/bin/env python3

import random
from subprocess import PIPE, Popen
from itertools import islice, zip_longest

import sys

sys.path.append("/Users/jlmccann0908/Dropbox/python_workspace/")
sys.path.append("/home/melampodium2/Dropbox/python_workspace/")
sys.path.append("/home/mccannj/Dropbox/python_workspace")
sys.path.append("/home/jamie/Dropbox/python_workspace")

def grouper(n, iterable):
	it = iter(iterable)

	while True:
		chunk = tuple(islice(it, n))

		if not chunk: return
		yield chunk


def grouper_witer(n, iterable, padvalue=None):
	"""grouper(3, 'abcdefg', 'x') -->
	('a','b','c'), ('d','e','f'), ('g','x','x')"""
 
	return zip_longest(*[iter(iterable)]*n, fillvalue=padvalue)

def cmd_line(command):
	""" Utility func for catching output from shell commands such
	    as grep, cut, cat, etc... """
	process = Popen(
		args=command,
		stdout=PIPE,
		shell=True
	)

	return process.communicate()[0]


def iter_sample_fast(iterable, samplesize):
	results = []
	iterator = iter(iterable)
	# Fill in the first samplesize elements:
	try:
		for _ in range(samplesize):
			results.append(next(iterator))
	except StopIteration:
		raise ValueError("Sample larger than population.")
	random.shuffle(results)  # Randomize their positions
	for i, v in enumerate(iterator, samplesize):
		r = random.randint(0, i)
		if r < samplesize:
			results[r] = v  # at a decreasing rate, replace random items
	return results


def main():
	return 0

if __name__ == '__main__':
	main()