#! /usr/bin/env python3

def load_cls(cls_file):

	fin = open(cls_file)
	cls_dict = {}
	current = ""
	for line in fin:
		line = line.rstrip("\n")
		if line.startswith(">"):
			current = line.lstrip(">").split(" ")[0]
		else:
			cls_dict[current] = line.split(" ")

	return cls_dict


def get_header(hitsort_counts_file):
	fin = open(hitsort_counts_file)
	header = fin.readline().rstrip("\n").split("\t")[1:]
	return header


def load_hitsort_counts(hitsort_counts_file, header):
	fin = open(hitsort_counts_file)
	hit_dict = {}

	fin.readline()

	for line in fin:
		ls = line.rstrip("\n").split("\t")
		read = ls[0]
		hits = ls[1:]
		hit_dict[read] = {header[x]:int(hits[x]) for x in range(len(header))}

	fin.close()
	return hit_dict


def calc_hsho(cls_dict, hitsort_dict, pre_length, outfile1, outfile2):
	
	fout1 = open(outfile1, "w")
	fout2 = open(outfile2, "w")
	header = "\t".join(["ID", "hs_count", "ho_count", "lineage"])
	print(header, file=fout1)

	for lineage in cls_dict:
		print("lineage: %s" % lineage)
		for read in cls_dict[lineage]:
			read_counts = hitsort_dict[read]
			ids = sorted(read_counts.keys())
			prefix = read[0:pre_length]
			hs = read_counts[prefix]
			ho = sum([read_counts[key] for key in read_counts if key != prefix])
			output1 = "\t".join([read, str(hs), str(ho), lineage])
			print(output1, file=fout1)
			output2 = "\t".join([read] + [str(read_counts[x]) for x in ids] + [lineage])
			print(output2, file=fout2)
	fout1.close()
	fout2.close()

	return 0


def main():
	import argparse as ap
	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-c', "--clsfile", help="cls file by annotation",
						required=True, type=str)
	parser.add_argument('-H', "--hitsort", help="hitsort counts file, not hitsort file",
						required=True, type=str)
	parser.add_argument('-p', "--prefix", help="prefix length",
						required=True, type=int)
	parser.add_argument('-o', "--outfile", help="specify a prefix for two output files to be generated",
						required=True, type=str)
	parser.add_argument('-t', "--taxa", help="specify prefixes to be used (default=all)", nargs="+",
						required=False, type=str)
	
	args = parser.parse_args()

	cls_dict = load_cls(args.clsfile)

	header = get_header(args.hitsort)
	print("loading hitsort")
	hitsort = load_hitsort_counts(args.hitsort, header)
	print("finished loading hitsort")
	outfile1 = args.outfile + ".hsho.txt"
	outfile2 = args.outfile + ".hits.by.lineage.txt"

	calc_hsho(cls_dict, hitsort, args.prefix, outfile1, outfile2)




	return 0

if __name__ == '__main__':
	main()