#! /usr/bin/env python3

from bs4 import BeautifulSoup

import xlwt

from xlrd import open_workbook#, XL_CELL_EMPTY

import string, traceback

def ext_total_number_seqs_used(repexp_soup):
	""" The first <p> element in the html is:
		<p>Number of sequences used for clustering: 10000000</p>"""

	return int(repexp_soup.p.contents[0].split(": ")[1])

def ext_te_domains_table(repexp_soup):
	h2oi = "Summary of TE domain hits"
	for table in repexp_soup.find_all("h2"):
		h2match = table.contents[0].rstrip("\n").lstrip()
		if h2match == h2oi: return table.find_next("tr")
		

def parse_domains_table(domains_table, cl_dict, cl_name, curr_num_reads):
	
	# first "tr" is not the one we want, hence [1:]
	cl_dict[cl_name] = {}
	for tr in domains_table.find_all("tr")[1:]:
		tds = list(tr.find_all("td"))
		# next lines simply follow the table structure in the html
		hit = int(tds[5].contents[0].rstrip("\n").lstrip())
		domain = tds[1].contents[0].rstrip("\n").lstrip()
		lineage = tds[4].contents[0].rstrip("\n").lstrip()
		tdoi = tds[3]
		lineage_formatted = format_lineage(lineage, domain, tdoi)
		key = "%s_%s_%s" % lineage_formatted

		# hit/curr_cl_num_reads*100 gives easier-to-read percentage
		if key not in cl_dict[cl_name]:
			cl_dict[cl_name][key] = (hit/curr_num_reads*100)

		else:
			# key is already in the dict
			cl_dict[cl_name][key] += (hit/curr_num_reads*100)
	return cl_dict

def format_lineage(lineage, domain, td):

	all_lines = ['DTM', 'DHH', 'DTA', 'DTH', 'DTC', 'DTT', 'DTx', 'DHx']

	if lineage == "":
		lineage = domain.split("-")[0]

	if lineage in all_lines:
		eltype = "DNA"

	elif lineage == "PARA" or lineage == "LINE":
		eltype = lineage

	else:
		eltype = td.contents[0].rstrip("\n").lstrip()


	return (eltype, lineage, domain)


def parse_cluster_connections(repexp_soup):
	connex = []
	for val in repexp_soup.find_all('img'):
		#print(val)
		clusters = [int(x[2:]) for x in val.parent.find_previous_sibling("p").contents[0].split()]
		#print(clusters)
		#cl_connex.append(val.parent.find_previous_sibling("p").contents[0].split())
		connex.append(clusters)
	return connex


def annotation_template(gsize, nclusts, nreads, clreads_list, domain, phy, others, notes, connects, outfile):

	lookup_cells = {
					"1/1":"1C[Gbp]=", "1/2":"total:", "1/3":"w/o cont.:", "2/3":xlwt.Formula("B2-K6"),
					"2/5":"nodes", "3/5":"[%]", "5/5":"domains", "6/5":"domain(s)", "6/6":"phylog.", "7/5":"other",
					"7/6":"(LTRs,...)", "8/5":"paired-end", "8/6":"groups", "9/5":"notes", "10/5":"classification",
					"11/5":"Contamination", "12/5":"Copia", "13/5":"Gypsy", "14/5":"PARA", "15/5":"LINE", "16/5":"DNA-TRP",
					"17/5":"rDNA", "18/5":"SATs", "19/5":"Unclassified", "20/5":"Unmarked"
				   }

	search_terms = ["Contamination","LTR/Copia","LTR/Gypsy","PARA","LINE","DNA/","rDNA","Satellite","Unclassified", "NULL"]
	search_locs = ["K","L","M","N","O","P","Q","R","S","T"]

	workbook = xlwt.Workbook()
	worksheet = workbook.add_sheet("Annotation")
	worksheet.write(0,1,gsize)
	worksheet.write(1,1,nreads)
	for x in range(7,7+nclusts):

		worksheet.write(x-1, 0, x-6)
		worksheet.write(x-1, 1, clreads_list[x-7])
		worksheet.write(x-1, 2, xlwt.Formula("B%s/$B$3" % x))
		worksheet.write(x-1, 4, domain[x-7])
		worksheet.write(x-1, 5, phy[x-7])
		worksheet.write(x-1, 6, others[x-7])
		worksheet.write(x-1, 8, notes[x-7])

		for y in range(10,20):
			formel = "IF(ISNUMBER(SEARCH(\"%s\",J%s)), B%s,0)" % (search_terms[y-10], x, x)
			worksheet.write(x-1, y, xlwt.Formula(formel))

	for key in lookup_cells.keys():
		x,y = int(key.split("/")[0]), int(key.split("/")[1])
		worksheet.write(y-1,x-1, lookup_cells[key])

	for x in range(0, len(search_locs)):
		col = search_locs[x]
		formel = "SUM(%s7:%s%s)" % (col, col, nclusts+6)
		worksheet.write(5, x+10, xlwt.Formula(formel))

	counter = 0
	group_dict = {}
	for group in connects:
		key = "group_%s" % counter
		ann_list = []
		for cls in group:
			#print(phy[cls-1], cls, cls+5, 7)
			#group_dict[key] = [[], cls+5, 7]
			ann_list.append((phy[cls-1], cls+5, 7))
			#worksheet.write(cls+5, 7, "group_%s" % counter)
		counter += 1
		#print("printing ann_list")
		#print(ann_list)
		group_dict[key] = ann_list

	for key in group_dict.keys():

		curr_lines = [x[0] for x in group_dict[key]]
		#print(curr_lines)
		uniq = set([x for x in curr_lines if x != ""])
		line_counter_list = []
		for val in uniq:
			line_counter_list.append((val, curr_lines.count(val)))
		#print(line_counter_list)
		line_sorted = sorted(line_counter_list, key=lambda x:x[1], reverse=True)
		#print(line_sorted)
		num_lines = len(line_sorted)
		if num_lines == 1:
			top_hit = line_sorted[0][0]
			#group_dict[key][0] = top_hit
			#print(group_dict[key])
			#print(type(group_dict[key]))
			group_dict[key].insert(0, top_hit)
			#print(group_dict[key])
			print("top hit for this group %s: %s" % (key, top_hit))
		elif num_lines > 1:
			top_hit = line_sorted[0][0]
			print("ambiguity found for this group, top hit for %s taken to be: %s" % (key, top_hit))
			#group_dict[key][0] = top_hit
			#print(group_dict[key])
			#print(type(group_dict[key]))
			group_dict[key].insert(0, top_hit)
			#print(group_dict[key])
		else:
			print("no top hit found for group %s, recommend unclassified" % key)
			#group_dict[key][0] = "Unclassified"
			#print(group_dict[key])
			#print(type(group_dict[key]))
			group_dict[key].insert(0, "Unclassified")
			#print(group_dict[key])


	line_counter = {}
	for key in group_dict.keys():
		curr_key = group_dict[key][0]
		if curr_key not in line_counter:
			line_counter[curr_key] = 0
			group_dict[key][0] = group_dict[key][0].lower().title() + "-A"
		else:
			line_counter[curr_key] += 1
			group_dict[key][0] = group_dict[key][0].lower().title() + "-%s" % string.ascii_uppercase[line_counter[curr_key]]
			#group_dict[key][0] += "-%s" % string.ascii_uppercase[line_counter[curr_key]]
		cell_coords = [(x[1],x[2]) for x in group_dict[key][1:]]
		for val in cell_coords:
			worksheet.write(val[0], val[1], group_dict[key][0])

	worksheet.col(3).width = 1000
	worksheet.col(4).width = 7500
	worksheet.col(5).width = 4000
	worksheet.col(7).width = 4000
	worksheet.col(9).width = 7500
	worksheet.col(10).width = 4500

	workbook.save(outfile)

	return 0


def main():

	import argparse as ap

	parser = ap.ArgumentParser()
	parser.add_argument('-i', '--infile', help="name (location + fn) of html file to be parsed",
						required=True, type=str)
	parser.add_argument('-g', '--genomesize', help="species genome size",
						required=False, type=float, default=1.00)
	parser.add_argument('-o', '--outfile', help="outfile name for spreadsheet",
						required=True, type=str)
	parser.add_argument('-n', '--ncolfile', help="full path to ncolInfo.txt (in clustering directory)",
						required=True, type=str)
	args = parser.parse_args()

	f = open(args.infile)
	ncol = open(args.ncolfile)
	ncol.readline()
	ncol_dict = {}
	for line in ncol:
		line = line.rstrip("\n").split("\t")
		ncol_dict[line[0]] = int(line[1])
		#print(line[0],line[1])


	# first things to do: read in full html and parse with BeautifulSoup
	# hope to be able to remove this dependency at some point
	# would have to write my own html parser for that
	html_text = f.read()
	soup = BeautifulSoup(html_text)

	# The first <p> tag in the html is:
	# <p>Number of sequences used for clustering: XXXXXXXXX</p>
	total_reads = ext_total_number_seqs_used(soup)
	print("\n >> The number of reads used for clustering: %s\n" % total_reads)

	# this cl_dict is for associating single clusters with their TE hits
	# { CL0001: { "Ty3/gypsy_chromovirus_Ty3-INT":450, ...} ...}
	cl_dict = {}
	# a list of the read numbers per cluster ... used for inserting into .xls annotation file
	read_nums = []

	for aelement in [x for x in soup.find_all('a') if "cluster.html" in x.get('href')]:
		html_file = aelement.get("href")
		cl_name = aelement.contents[0]#html_file.split("/")[0]
		
		curr_html = open(html_file)
		curr_soup = BeautifulSoup(curr_html.read())
		curr_num_reads = ncol_dict[cl_name]
		read_nums.append(curr_num_reads)
		print(html_file, cl_name, curr_num_reads)
		print("cluster html file location: %s" % html_file)
		print("number of reads: %s " % curr_num_reads)
		print("opening file for parsing: %s" % html_file)
		cl_dict[cl_name] = {}

		# the third <h2> in each ind. cluster html is the tag occurring just
		# before the TE domains hits table which needs to be parsed
		try:
			domains_table = ext_te_domains_table(curr_soup)
			cl_dict = parse_domains_table(domains_table, cl_dict, cl_name, curr_num_reads)
		except (IndexError, AttributeError):
			#traceback.print_exc()
			pass

		print("closing parsed file: %s" % html_file)
		print("")
		curr_html.close()
	
	f.close()

	domains = []
	domains_phy = []
	other = []
	notes = []
	for cls_key in sorted(cl_dict, key=lambda x: int(x[2:])):
		print(cls_key)
		line_dict = {}
		for line_key in sorted(cl_dict[cls_key], key=cl_dict[cls_key].get, reverse=True):
			if cl_dict[cls_key][line_key] > 1.00:
				#print(line_key, cl_dict[cls_key][line_key])
				line = "_".join(line_key.split("_")[0:2])
				if line not in line_dict:
					line_dict[line] = [cl_dict[cls_key][line_key], [line_key.split('-')[1]]]
				else:
					line_dict[line][0] += cl_dict[cls_key][line_key]
					line_dict[line][1].append(line_key.split('-')[1])
		hits = [] 
		for akey in sorted(line_dict, key=lambda x: line_dict[x][0]):
			#print(akey, line_dict[akey])
			hits.append((akey, line_dict[akey][0]))

		hits_sort = sorted(hits, key=lambda x: x[1], reverse=True)

		try:
			ambiguous = hits_sort[1][1]/hits_sort[0][1] >= 0.8 
		except IndexError:
			ambiguous = 0
			pass

		printed = False

		if len(hits_sort) == 0:
			notes.append("")
			domains_phy.append("")
			other.append("")
			domains.append("")

		num_vals = 0
		for key,val in hits_sort:
			if val >= 5.00 and not(printed):
				num_vals += 1
				if ambiguous:
					notes.append("ambiguous")
				else:
					notes.append("")
				print(key, val)

				domains_phy.append(key.split("_")[1].upper())
				other.append(key.split("_")[0].upper())
				domains.append(",".join(sorted(line_dict[key][1])).upper())
				printed = True
		if num_vals == 0 and len(hits_sort) > 0:
			notes.append("")
			domains_phy.append("")
			other.append("")
			domains.append("")

	connex_html = open("merge_cutoff0.1/mate_pairs_merging.html")
	print("getting connections soup")
	soup = BeautifulSoup(connex_html.read())
	cl_connex = parse_cluster_connections(soup)

	num_clusts = int(cl_name[2:])
	annotation_template(args.genomesize, num_clusts, total_reads, read_nums, domains, domains_phy, other, notes, cl_connex, args.outfile)

	return 0

if __name__ == '__main__':
	main()