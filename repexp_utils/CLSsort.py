#! /usr/bin/env python3

###########
# this script provides way to assign reads from clusters to specific annotated lineages from manual annotation
# annotation and original cls file from analysis must be provided
# annotation must be in format from Jiri's comparative analysis annotation text output
# it looks like this:
# CL1	95.5	Maximus/Sire	3.2 Ivana/Oryco
# Final annotation must be in 3rd column, middle number does not matter
# CL number must be in first column, second column must be present. Just make one up if necessary
###########


def build_annotation(anno_file):
	fin = open(anno_file)
	anno_dict = {}
	for line in fin:
		line = line.rstrip("\n")
		ls = line.split("\t")
		if ls[2] != "x":
			anno_dict[ls[0]] = ls[2]
		else:
			anno_dict[ls[0]] = "Unclassified"
	return anno_dict

def build_cls(cls_file, max_cluster):
	fin = open(cls_file)
	cls_dict = {}
	current = ""
	for line in fin:
		line = line.rstrip("\n")
		if line.startswith(">"):
			current = line.lstrip(">").split(" ")[0]
			cl_num = int(current[2:])
			if cl_num > max_cluster: break
		else:
			cls_dict[current] = line.split(" ")

	return cls_dict

def merge_cluster_read_ids(annotation, old_cls):
	final_dict = {}
	for key in annotation.keys():
		if annotation[key] not in final_dict:
			final_dict[annotation[key]] = old_cls[key]
		else:
			final_dict[annotation[key]] += old_cls[key]
	return final_dict


def write_new_cls(final_cls, outf):
	fout = open(outf, "w")

	for key in final_cls:
		name = ">%s" % key
		joined = " ".join(final_cls[key])
		print(name, joined, file=fout, sep="\n")

	return 0


def main():
	import argparse as ap
	
	parser = ap.ArgumentParser()
		
	parser.add_argument('-a', "--annotation", help="annotation to be analyzed",
						required=True, type=str)
	parser.add_argument('-c', "--clsfile", help="cls file to be sorted",
						required=True, type=str)
	parser.add_argument('-o', "--outfile", help="output file name",
						required=True, type=str)
	parser.add_argument('-m', "--maxcluster", help="highest cluster number in annotation",
						required=True, type=int)
	
	args = parser.parse_args()

	annotation = build_annotation(args.annotation)

	for val in range(1, args.maxcluster+1):
		print("CL%s" % val, annotation["CL%s" % val])

	cls = build_cls(args.clsfile, args.maxcluster)

	for val in range(1, args.maxcluster+1):
		print("CL%s" % val, len(cls["CL%s" % val]))

	new_cls = merge_cluster_read_ids(annotation, cls)

	for key in new_cls.keys():
		print(key, len(new_cls[key]))

	write_new_cls(new_cls, args.outfile)

	return 0



if __name__ == '__main__':
	main()